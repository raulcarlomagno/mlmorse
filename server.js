var express = require('express');
var app = express();
var bodyParser = require('body-parser')
var morseDecoder = require('./morse_decoder');

app.use(bodyParser.json())

app.post('/translate/2text', function (req, res) {
    
   let morseChars = req.body.text.split(' ');

    var finalBits = morseChars.map(el => {
        bits = el.split('').map(e => (e == '.' ? '10' : (e == '-' ? '1110' : ''))).join('');
        bits += '000';
        return bits;
    }).join('');

    res.send(morseDecoder.translate2Human(morseDecoder.decodeBits2Morse(finalBits)));
});

app.post('/translate/2morse', function (req, res) {
    res.send(morseDecoder.translate2Morse(req.body.text));
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, function () {
  console.log('MLMorse listening on port ' + PORT);
});